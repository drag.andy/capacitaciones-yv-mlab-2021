* ###  *[Regresar al menu](https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/blob/master/README.md)*.

 # Menu
- [Como_acceder_a_Gitlab](#como_acceder_a_Gitlab )
- [Crear_repositorios](#crear_repositorios)
- [Crear_issues](#crear_issues)
- [Crear_labels](#crear_labels)
- [Como_agregar_miembros](#como_agregar_miembros)
- [Crear_boards_y_manejo_de_boards](#crear_boards_y_manejo_de_boards)
- [Permisos_que_existen](#permisos_que_existen)
- [Crear_ramas_desde_gitlab](#crear_ramas_desde_gitlab)
- [Como_crear_commit](#como_crear_commit)
- [Crear_ramas_desde_gitlab](#crear_ramas_desde_gitlab)
- [Crear_grupos](#crear_grupos)
- [Crear_subGrupos](#crear_subGrupos)





# Como_acceder_a_Gitlab
- [ir al inicio](#menu)

Gitlab es un servicio web de control de versiones y desarrollo de software colaborativo basado en Gity Además es un  gestor de repositorios y otras cosas más.
<br>

+ En cualquier de los navegadores preferidos accedemos  a Gitlab, se puede registrar, creando una cuenta nueva, o logearse directamente desde el correo electrónico, Twitter, Github entre otros. 

   <img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto1.png" width="700">

<br>

# Crear_repositorios
- [ir al inicio](#menu)

+  Para crear un repositorio se debe accder a crear un New Proyect.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto2.png" width="700">

+  Una vez realizado, se abrirá una ventana donde se debe poner el nombre del repositorio, y elegir si quiere que sea público o privado.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto3.png" width="700">


# Crear_grupos
- [ir al inicio](#menu)

Un grupo de gitLab es un conjunto de proyectos, junto con los datos acerca de los usuarios que tienen acceso. Es importnte reconocer que cada grupo tiene también un espacio de nombres específico 


+ Pra crear un grupo se debe dar clic en la opcion de Groups y en create group

 <img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto33.png" width="700">


+ Una vez realizado se desplegara una nueva pantalla para llenar los datos correspondientes.

 <img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto34.png" width="700">

+ Culminado eso dar clic en aceptar y aparecera un mensaje de grupo creado.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto35.png" width="700">


# Crear_subGrupos
- [ir al inicio](#menu)

+ En la misma sección existe un boton para crear un subgrupo como la imagen que s emuestra a continuación.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto36.png" width="700">


+ Dra clic y y se abre una nueva ventana  para llenar los datos uan vez creado el subgrupo, se mostraria de esta manera.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto37.png" width="700">

# Crear_issues
- [ir al inicio](#menu)

+ Un Issue practicamente, en esapñaol quiee decir problema, ahi se puede espeficar o desglosarlo en partes.

+ Para crear un Issue se debe acceder a Issue y se nos desplegara una lista de opciones.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto4.png" width="700">

+ Para crear un issue se debe acceder a "new Issue"

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto6.png" width="700">

+ Se debe escribir el tema y la especificación del mismo una vez realizado nos aparecera la lista de los issue creados como la siguiente.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto5.png" width="700">



# Crear_labels
- [ir al inicio](#menu)

+ Los labels sirve para tener una mejor organización  y poderlos separa  en este ejemplo se realizan dos un To do, Doing y Done.

+ Para crear un labels se deber dar clic en new labels y se mostrara una pantalla como la siguiente.  

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto7.png" width="700">

+ Una vez realizado aquello se puede añadir aun issue para mayor conocimiento de aquella tarea. 

# Crear_boards_y_manejo_de_boards
- [ir al inicio](#menu)

+ Los boards son tableros que ayudan a tener una mejor estructura organizada, para ello se debe dar clic en "create new board".

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto8.png" width="700">

+ Una vez creado los labels creados previamente los añadimos  y se nos mostraran unos tables como los siguientes.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto9.png" width="700">


# Como_agregar_miembros
- [ir al inicio](#menu)

+ Para agregar miembros se debe dar clic en "Members".

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto10.png" width="700">

+ Una vez realizado  en la sección de "GitLab member or Email address", se debe ingresar el usuario para añadirlo.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto10.png" width="700">

# Permisos_que_existen
- [ir al inicio](#menu)

+ Los premiso que existen son: Guest solo puede ver o examinar  a comparacion de los demás , el Report  no puede invitar a otros miembros, Developer no puede  crear grupos , y el Maintainer no puede  eliminar los grupos o repositorio y se los puede dar cuando  agregemos miembros al repositorio. 

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto11.png" width="700">

Roles y funciones

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto12.png" width="700">
<br>

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto13.png" width="700">


# Como_crear_commit
- [ir al inicio](#menu)

+ El comando git commit captura una instantánea de los cambios preparados en ese momento del proyecto.

+ para realzair commit se debe editar un archivo que se desee.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto14.png" width="700">


+ Se especifíca el commit o los cambios realizados y se da clic  en "commit changes ".

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto15.png" width="700">

y se guardaran los cambios realizados.

# Crear_ramas_desde_gitlab
- [ir al inicio](#menu) 

Una rama Git es simplemente un apuntador móvil apuntando a una de esas confirmaciones. La rama por defecto de Git es la rama master . Con la primera confirmación de cambios que realicemos, se creará esta rama principal master apuntando a dicha confirmación.

+ Para crear una rama se debe acceder a "New branch".

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto16.png" width="700">

+ Escribimos el nombre de la rama, de preferencia , amenudo que se creamos debe ir con  secuencia por ejemplo rama1, rama2 etc, luego el usuario debe dar clic en "crear rama".

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto17.png" width="700">

+ Y de esa manera ya se mostraran dos ramas.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto18.png" width="700">
