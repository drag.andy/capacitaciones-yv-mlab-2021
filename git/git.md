* ###  *[Regresar al menu](https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/blob/master/README.md)*.

# Menu
- [Que_es_git](#que_es_git)

- [Comandos_de_git_en_consola](#comandos_de_git_en_consola)

- [Clientes_git](#clientes_git)

- [Clonación_de_proyecto_por_consola_y_cliente](#clonación_de_proyecto_por_consola_y_cliente)

- [Commit_por_consola_y_clientes_krake_y_smart](#commit_por_consola_y_clientes_krake_y_smart)

- [Ramas_desde_kraken](#ramas_desde_kraken)



# Que_es_git
- [ir al inicio](#menu)

Git es una herramienta que realiza una función del control de versiones de código de forma distribuida, de la que destacamos varias características:.
•	Tiene un sistema de trabajo con ramas que lo hace especialmente potente.

•	En cuanto a la funcionalidad de las ramas, las mismas están destinadas a provocar proyectos divergentes de un proyecto principal, para hacer experimentos o para probar nuevas funcionalidades.


•	Las ramas pueden tener una línea de progreso diferente de la rama principal donde está el core de nuestro desarrollo. En algún momento podemos llegar a probar algunas de esas mejoras o cambios en el código y hacer una fusión a nuestro proyecto principal, ya que todo esto lo maneja Git de una forma muy  eficiente.

•	Es muy potente


•	Es software libre

•	Con ella podemos mantener un historial completo de versiones


•	Podemos movernos, como si tuviéramos un puntero en el tiempo, por todas las revisiones de código y desplazarnos una manera muy ágil.


# Comandos_de_git_en_consola
- [ir al inicio](#menu)

Comandos  principales

git help

Muestra una lista con los comandos más utilizados en GIT.

+ git init
Podemos ejecutar ese comando para crear localmente un repositorio con GIT y así utilizar todo el funcionamiento que GIT ofrece.  Basta con estar ubicados dentro de la carpeta donde tenemos nuestro proyecto y ejecutar el comando.  Cuando agreguemos archivos y un commit, se va a crear el branch master por defecto.

+ git add + nombre
El comando añade recursivamente los archivos que están dentro de él.

+ git commit -m "mensaje" + archivos
Hace commit a los archivos que indiquemos, de esta manera quedan guardados nuestras modificaciones.

+ git commit -am "mensaje"
Hace commit de los archivos que han sido modificados y GIT los está siguiendo.

+ git status
Nos indica el estado del repositorio, por ejemplo cuales están modificados, cuales no están siendo seguidos por GIT, entre otras características.

+ git clone URL/name.git NombreProyecto
Clona un proyecto de git en la carpeta NombreProyecto.

+ Git pull
Para bajar los cambios que se hayan realizado  desde otro sitio 

+ Git push
Para subir los cambios hechos a la rama master 

#### Acontinuacion un ejemplo de como se realiza en consola.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto22.png" width="700">


# Clientes_git
- [ir al inicio](#menu)

+ los clientes de l git conocidos son gitkraken  y Smartgit.

##  GitKraken

GitKraken es utilizado por empresas como Netflix, Tesla y Apple, es la base para los desarrolladores que están en busca de una interfaz más amigable para Git, con integraciones a GitHub, gitlab, bitbucket y VSTS (Azure DevOps).

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto19.png" width="700">

##  Smartgit

SmartGit es una herramienta multiplataforma de entorno fácil e intuitivo que nos ayuda con Git. Veamos como empezar a trabajar con esta herramienta usando Git y el alojamiento GitHub. 
<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto20.png" width="700">



# Clonación_de_proyecto_por_consola_y_cliente
- [ir al inicio](#menu)

+ Para clonar un repositorio desde consola se pone git clone y el enlace del repositorio.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto21.png" width="700">

+ Para clonar un repositorio desde un cliente como kraken accedemos a la app.

+ Nos direccionamos a clone.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto23.png" width="700">

+ Se lo puede realizar desde el gitlab o github, o también se lo puede realizar con el link que proporcionan.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto24.png" width="700">

+ Una vez realizado  se  da click en "clone de repo!" y se abrira el archivo .

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto25.png" width="700">




# Commit_por_consola_y_clientes_krake_y_smart
- [ir al inicio](#menu)


+ Para realizar un commit por consola se lo realiza de esta manera: primero se agrega los archivos que se deseen, con el git add .

+ Luego se realiza un commit con el comando siguiente: git commit -m "aqui va el comentario".

+ Acontinuacion un ejemplo
<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto26.png" width="700">

+ Para realizar un commit desde un cliente en este caso gitKraken, cuando se haya realizado modificaciones se debe acceder a la parte derecha, ahi se ven los cambios y se puede escoger  las modificaciones  que desee subir a las ramas.


<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto27.png" width="700">

+ Una vez seleccioando todos los archivos se nos bajan a la parte inferior.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto28.png" width="700">

+ luego se escribe el comentario del commit.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto29.png" width="700">





# Ramas_desde_kraken
- [ir al inicio](#menu)

+ Para crear una rama en Gitkraken se debe seleccionar el commit desde el cual iniciará, ya que se copiarán todos los archivos existentes y su estado en dicha versión.

+ Para ello se hace clic izquierdo sobre la representación de éste en la parte central de la ventana de trabajo.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto30.png" width="700">

+ O de la misma manera se puede  dar clic en el botón "Branch" e inmediatamente se desplegara un input para poner el nombre de la rama.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto31.png" width="700">


+ De esta manera ya se puede visualizar  la rama creada en el área principal, de la msima manera aparece un listado de repositorios locales.

<img src="https://gitlab.com/drag.andy/capacitaciones-yv-mlab-2021/-/raw/master/imagen/foto32.png" width="700">


+ Es importante recalcar que la rama sólo se encuentra en su repositorio local, ya que en el repositorio en el original  aún no se genera esa rama, por lo que es necesario dar a conocer  la generación de la misma.

+ Se abre un menú haciendo clic en el nombre del branch,una vez realizado se acepta la confirmación.